file_handler = open("library-card.txt", "r") # open takes a file name and a mode (read, write, append, delete?)
file_contents = file_handler.read() # save contents of library_card.txt to variable called file_contents
file_handler.close()	

lines = file_contents.split("\n") # break on the \n characters

call_number = lines[0]
print "Call number: " + call_number

author = lines[1]
print "Author: " + author

title = lines[2]
print "Title: " + title 

due_dates = lines[3:] # want all lines from 3 to the end of the list. onward!
print "Due dates: " + str(due_dates) 


months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"] # list of month abbreviations

clean_data = [] # create a list to save out clean data


for date in due_dates:
	date_parts = date.split()
	month = date_parts[0]
	day = date_parts[1]
	year = date_parts[2]

	print "-" # this is a separator
	print "Month: " + month
	print "Day: " + month
	print "Year: " + year


	if year.startswith("'"):
		new_year = "19" + year[1:] # deal with years with single quote in front (e.g. '59)
	elif len(year) == 2:
		new_year = "19" + year # deal with years that are only two digits (e.g. 60)
	else:
		new_year = year 

	print "New year: " + new_year
	assert len(new_year) == 4 # check to make sure year is four digits


	# convert month names to numbers
	assert month in months, "Invalid month name"
	month_index = months.index(month)
	new_month = month_index + 1
	print "New month: " + str(new_month)

	# clean_data.append([new_year, str(new_month), day]) # add cleaned data to the list

	cleaned_due_date = new_year + "-" + str(new_month) + "-" + day
	clean_data.append(cleaned_due_date)

print "Data successfully processed!"
print "Clean data: " + str(clean_data)

# save the nicely formatted data

file_handler = open("library-card-clean.txt", "w") 
file_handler.write(call_number + "\n")
file_handler.write(author + "\n")
file_handler.write(title + "\n")

for data in clean_data:
	file_handler.write(data + "\n")

file_handler.close()	

print "All done, good job!"